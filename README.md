# HOW TO USE THIS SCRIPT #

### Preconditions ###
You should have 2 files: file A and file B.

***File A*** is positions.txt which is used for the iPad device, ***file B*** is positions.txt which you are going to use for iPhone4S/5/5S

**Both files have to have exactly the same amount of lines!**

### Steps ###
* For the best user experience, open up the project and build the application, then locate the product in Finder and run it from Terminal app
* Drag the first file - file A - to the Terminal window
* Drag the second file - file B - to the terminal window
* Ta-daaam - you get file **c.txt** at the same location where you **file B** is
* Use your new and awesome c.txt file