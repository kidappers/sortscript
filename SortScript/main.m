//
//  main.m
//  SortScript
//
//  Created by Yaroslav Arsenkin on 7/22/14.
//  Copyright (c) 2014 Yaroslav Arsenkin. All rights reserved.
//

#import <Foundation/Foundation.h>

NSString *getPathToFiles(NSString *incomePath) {
    NSURL *fullPathWithFilename = [NSURL URLWithString:incomePath];
    NSMutableArray *pathByComponents = (NSMutableArray*)[fullPathWithFilename pathComponents];
    
    [pathByComponents removeLastObject];
    [pathByComponents removeObjectAtIndex:0];
    
    NSString *path = [NSString stringWithFormat:@"/%@", [pathByComponents componentsJoinedByString:@"/"]];

    return path;
}

void writeToFile(NSString *content, NSString *file) {
    BOOL fileWriteSuccess = [content writeToFile:[NSString stringWithFormat:@"%@/c.txt", getPathToFiles(file)]
                                                    atomically:YES
                                                      encoding:NSUTF8StringEncoding
                                                         error:nil];
    if (fileWriteSuccess) {
        NSLog(@"SUCCESS!");
    } else {
        NSLog(@"OOPS!");
    }
}

int main(int argc, const char * argv[])
{

    @autoreleasepool {
        
        NSLog(@"Drag original file here!");
        char str[] = "";
        scanf("%s", str);
        NSString *filePath = [NSString stringWithUTF8String:str];
        
        NSString *fileContent1 = [NSString stringWithContentsOfFile:filePath encoding:NSUTF8StringEncoding error:nil];
        
        NSMutableArray *spriteNames = [[NSMutableArray alloc] init];
        for (NSString *line in [fileContent1 componentsSeparatedByCharactersInSet:[NSCharacterSet newlineCharacterSet]]) {
            NSArray *array = [line componentsSeparatedByString:@","];
            [spriteNames addObject:[array firstObject]];
        }
        
        NSLog(@"Drag second file here!");
        scanf("%s", str);
        filePath = [NSString stringWithUTF8String:str];
        
        NSString *fileContent2 = [NSString stringWithContentsOfFile:filePath encoding:NSUTF8StringEncoding error:nil];
        
        NSMutableDictionary *spriteNamesAndPositions = [[NSMutableDictionary alloc] init];
        for (NSString *line in [fileContent2 componentsSeparatedByCharactersInSet:[NSCharacterSet newlineCharacterSet]]) {
            NSArray *array = [line componentsSeparatedByString:@","];
            NSMutableArray *newArray = [NSMutableArray arrayWithArray:array];
            [newArray removeObjectAtIndex:0];
            NSString *positions = [newArray componentsJoinedByString:@","];
            NSString *spriteName = [array firstObject];
            
            [spriteNamesAndPositions setObject:spriteName forKey:spriteName];
            [spriteNamesAndPositions setValue:positions forKey:spriteName];
        }
        
        NSMutableString *newCompleteFileString = [[NSMutableString alloc] init];
        for (NSString *spriteName in spriteNames) {
            [newCompleteFileString appendFormat:@"%@,%@\n", spriteName, [spriteNamesAndPositions valueForKey:spriteName]];
        }
        
        writeToFile(newCompleteFileString, filePath);
    }
    
    return 0;
}

